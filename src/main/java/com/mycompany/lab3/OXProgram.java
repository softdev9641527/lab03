/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

/**
 *
 * @author Asus
 */
class OXProgram {

    static boolean checkWin(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer)) {
            return true;
        }
        else if (checkCol(table, currentPlayer)) {
            return true;
        }
        else if (checkX1(table, currentPlayer)) {
            return true;
        }
        else if (checkX2(table, currentPlayer)) {
            return true;
        }
        else if (checkDraw(table)) {
            return true;
        }
        return false;

    }

    private static boolean checkRow(String[][] table, String currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(table, currentPlayer, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }

    private static boolean checkCol(String[][] table, String currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(table, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);
    }
    
    private static boolean checkX1(String[][] table, String currentPlayer) {
        for (int x1 = 0; x1 < 3; x1++) {
            if (checkX1(table, currentPlayer, x1)) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkX1(String[][] table, String currentPlayer, int x1) {
        return table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer);
    }

    private static boolean checkX2(String[][] table, String currentPlayer) {
        for (int x2 = 0; x2 < 3; x2++) {
            if (checkX2(table, currentPlayer, x2)) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkX2(String[][] table, String currentPlayer, int x2) {
        return table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][0].equals(currentPlayer);
    }

    private static boolean checkDraw(String[][] table) {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col] == "-") {
                    return false;
                }
            }
        }
        return true;
    }

}
